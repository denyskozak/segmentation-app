import numpy as np
import matplotlib.pyplot as plt

img_num = 2

# Select static slice
n_slice = 50

# Load saved files
val_img = np.load(f"../predictions/val_img_{img_num}.npy")
val_gt = np.load(f"../predictions/val_gt_{img_num}.npy")
val_pred_baseline = np.load(f"../predictions/val_pred_Baseline_{img_num}.npy")
val_pred_deepSup = np.load(f"../predictions/val_pred_DeepSup_{img_num}.npy")
val_pred_swin_deepSup = np.load(f"../predictions/val_pred_Swin_DeepSup_{img_num}.npy")

val_gt = val_gt.argmax(axis = 1)
val_pred_baseline = val_pred_baseline.argmax(axis = 0)
val_pred_deepSup = val_pred_deepSup.argmax(axis = 1)
val_pred_swin_deepSup = val_pred_swin_deepSup.argmax(axis = 1)

# Split the combined image into 4 separate images
test_image_t1n = val_img[0, 0, :, :, :]



# Create a grid of subplots with 1 row and 5 columns
fig, axes = plt.subplots(1, 5, figsize=(20, 8))
#fig.suptitle('Image and Mask Visualization', fontsize=16)

# Image T1n
axes[0].imshow(test_image_t1n[:, :, n_slice], cmap="gray")
axes[0].set_title('Image T1n')

# Image T1c
axes[1].imshow(val_gt[0, :, :, n_slice], cmap=plt.cm.jet)
axes[1].set_title('Ground Truth')

axes[2].imshow(val_pred_baseline[:, :, n_slice], cmap=plt.cm.jet)
axes[2].set_title('Prediction AR2B')

axes[3].imshow(val_pred_deepSup[0, :, :, n_slice], cmap=plt.cm.jet)
axes[3].set_title('Prediction AR2B_DeepSup')

axes[4].imshow(val_pred_swin_deepSup[0, :, :, n_slice], cmap=plt.cm.jet)
axes[4].set_title('Prediction Swin_AR2B_DeepSup')

# Adjust the spacing between subplots
plt.tight_layout()

# Display the plot
plt.show()

mappable = plt.imshow(val_gt[0, :, :, n_slice], cmap=plt.cm.jet)
plt.colorbar(mappable, label='Intensity')


# Split the combined image into 4 separate images
test_image_t1n = val_img[0, 0, :, :, :]
test_image_t1c = val_img[0, 1, :, :, :]
test_image_t2w = val_img[0, 2, :, :, :]
test_image_flair = val_img[0, 3, :, :, :]

# Select static slice
n_slice = 50

# Create a grid of subplots with 1 row and 5 columns
fig, axes = plt.subplots(1, 5, figsize=(20, 8))
fig.suptitle('Image and Mask Visualization', fontsize=16)

# Image T1n
axes[0].imshow(test_image_t1n[:, :, n_slice], cmap="gray")
axes[0].set_title('Image T1n')

# Image T1c
axes[1].imshow(test_image_t1c[ :, :, n_slice], cmap="gray")
axes[1].set_title('Image T1c')

# Image T2w
axes[2].imshow(test_image_t2w[:, :, n_slice], cmap="gray")
axes[2].set_title('Image T2w')

# Image Flair
axes[3].imshow(test_image_flair[:, :, n_slice], cmap="gray")
axes[3].set_title('Image Flair')

# Mask
axes[4].imshow(val_gt[0, :, :, n_slice], cmap=plt.cm.jet)
axes[4].set_title('Mask')

# Adjust the spacing between subplots
plt.tight_layout()

# Display the plot
plt.show()