import pandas as pd, numpy as np
import matplotlib.pyplot as plt

# Load CSV files
df1 = pd.read_csv('17_training_results.csv')
df2 = pd.read_csv('08_training_results.csv')
df3 = pd.read_csv('09_training_results.csv')

# Convert Epoch to strings
df1['Epoch'] = df1['Epoch'].astype(str)
df2['Epoch'] = df2['Epoch'].astype(str)
df3['Epoch'] = df3['Epoch'].astype(str)

# Filter dataframe and epochs for training results
df1_train = df1[~df1['Epoch'].str.contains('Validation')]
epochs1_train = df1_train['Epoch']
df2_train = df2[~df2['Epoch'].str.contains('Validation')]
epochs2_train = df2_train['Epoch']
df3_train = df3[~df3['Epoch'].str.contains('Validation')]
epochs3_train = df3_train['Epoch']

# Extract starting epoch for training results
start1_train = int(epochs1_train.iloc[0])
start2_train = int(epochs2_train.iloc[0])
start3_train = int(epochs3_train.iloc[0])

# Calculate mean dice scores for training results
mean_dice1_train = df1_train[['Dice Coef (1)', 'Dice Coef (2)', 'Dice Coef (3)']].mean(axis=1)
mean_dice2_train = df2_train[['Dice Coef (1)', 'Dice Coef (2)', 'Dice Coef (3)']].mean(axis=1)
mean_dice3_train = df3_train[['Dice Coef (1)', 'Dice Coef (2)', 'Dice Coef (3)']].mean(axis=1)

# Filter dataframe and epochs for validation results
df1_val = df1[df1['Epoch'].str.contains('Validation')]
epochs1_val = df1_val['Epoch']
df2_val = df2[df2['Epoch'].str.contains('Validation')]
epochs2_val = df2_val['Epoch']
df3_val = df3[df3['Epoch'].str.contains('Validation')]
epochs3_val = df3_val['Epoch']

# Extract starting epoch for validation results
start1_val = 50 #int(epochs1_val.iloc[0].split()[1])
start2_val = 50 #int(epochs2_val.iloc[0].split()[1])
start3_val = 50 #int(epochs3_val.iloc[0].split()[1])

# Calculate mean dice scores for validation results
mean_dice1_val = df1_val[['Dice Coef (1)', 'Dice Coef (2)', 'Dice Coef (3)']].mean(axis=1)
mean_dice2_val = df2_val[['Dice Coef (1)', 'Dice Coef (2)', 'Dice Coef (3)']].mean(axis=1)
mean_dice3_val = df3_val[['Dice Coef (1)', 'Dice Coef (2)', 'Dice Coef (3)']].mean(axis=1)

# Create subplots
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))

# Assuming mean_dice1_train, mean_dice2_train, and mean_dice3_train are numpy arrays or lists
# you can convert them to numpy arrays if they are not
mean_dice1_train = np.array(mean_dice1_train)
mean_dice2_train = np.array(mean_dice2_train)
mean_dice3_train = np.array(mean_dice3_train)

# Now, you can take every 5th value from these arrays
mean_dice1_train_smooth = mean_dice1_train[::5]
mean_dice2_train_smooth = mean_dice2_train[::5]
mean_dice3_train_smooth = mean_dice3_train[::5]

# Then, you can plot the smoothed curves
ax1.plot(range(start1_train, len(epochs1_train) + start1_train, 5), mean_dice1_train_smooth, color='red', label='AR2B')
ax1.plot(range(start2_train, len(epochs2_train) + start2_train, 5), mean_dice2_train_smooth, color='blue', label='AR2B_DeepSup')
ax1.plot(range(start3_train, len(epochs3_train) + start3_train, 5), mean_dice3_train_smooth, color='green', label='Swin_AR2B_DeepSup')
ax1.set_title('Mean Training Dice Score')
ax1.set_ylabel('Mean Dice Score')
ax1.set_xlabel('Epoch')
ax1.set_ylim(bottom=0, top=1)
ax1.legend(loc='lower right')

# Plot validation mean dice score
ax2.plot(range(start1_val, len(epochs1_val) * 50 + start1_val, 50), mean_dice1_val, color='red', label='AR2B')
ax2.plot(range(start2_val, len(epochs2_val) * 50 + start2_val, 50), mean_dice2_val, color='blue', label='AR2B_DeepSup')
ax2.plot(range(start3_val, len(epochs3_val) * 50 + start3_val, 50), mean_dice3_val, color='green', label='Swin_AR2B_DeepSup')
ax2.set_title('Mean Validation Dice Score')
ax2.set_ylabel('Mean Dice Score')
ax2.set_xlabel('Epoch')
ax2.set_ylim(bottom=0, top=0.9)
ax2.legend(loc='lower right')

# Find max dice scores for training
max_dice1_train = mean_dice1_train.max()
max_dice2_train = mean_dice2_train.max()
max_dice3_train = mean_dice3_train.max()

# Find max dice scores for validation
max_dice1_val = mean_dice1_val.max()
max_dice2_val = mean_dice2_val.max()
max_dice3_val = mean_dice3_val.max()

# Annotate highest dice scores for training subplot
ax1.annotate(f'Max Dice: {max_dice1_train:.3f}', xy=(np.argmax(mean_dice1_train) + start1_train, max_dice1_train), xytext=(-40, -60),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax1.annotate(f'Max Dice: {max_dice2_train:.3f}', xy=(np.argmax(mean_dice2_train) + start2_train, max_dice2_train), xytext=(-70, 20),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax1.annotate(f'Max Dice: {max_dice3_train:.3f}', xy=(np.argmax(mean_dice3_train) + start3_train, max_dice3_train), xytext=(-60, 30),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')

# Annotate highest dice scores for validation subplot
ax2.annotate(f'Max Dice: {max_dice1_val:.3f}', xy=(np.argmax(mean_dice1_val) * 50 + start1_val, max_dice1_val), xytext=(-40, 60),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax2.annotate(f'Max Dice: {max_dice2_val:.3f}', xy=(np.argmax(mean_dice2_val) * 50 + start2_val, max_dice2_val), xytext=(-40, 20),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax2.annotate(f'Max Dice: {max_dice3_val:.3f}', xy=(np.argmax(mean_dice3_val) * 50 + start3_val, max_dice3_val), xytext=(-40, -40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')

# Check for re-training condition
retrain_condition = any([int(epochs1_train.iloc[-1])>1000,
                         int(epochs2_train.iloc[-1])>1000,
                         int(epochs3_train.iloc[-1])>1000])

# Show vertical lines if re-training
if retrain_condition:
  # Vertical line and text for epoch 1000 in training subplot
  ax1.axvline(x=1000, linestyle='--', color='grey')
  ax1.text(1010, ax2.get_ylim()[0] + 0.05, 'Extended Training', fontsize=9, color='grey')

  # Vertical line and text for validation epoch 1000 in validation subplot
  ax2.axvline(x=1000, linestyle='--', color='grey')
  ax2.text(1010, ax2.get_ylim()[0] + 0.05, 'Extended Training', fontsize=9, color='grey')



# Adjust layout
plt.tight_layout()

plt.show()