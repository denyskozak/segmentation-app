import pandas as pd
import matplotlib.pyplot as plt

# Load CSV files
df1 = pd.read_csv('07_training_results.csv')
df2 = pd.read_csv('08_training_results.csv')
df3 = pd.read_csv('09_training_results.csv')

# Convert Epoch to strings
df1['Epoch'] = df1['Epoch'].astype(str)
df2['Epoch'] = df2['Epoch'].astype(str)
df3['Epoch'] = df3['Epoch'].astype(str)

# Filter dataframe and epochs
train_df1 = df1[~df1['Epoch'].str.contains('Validation')]
train_epochs1 = train_df1['Epoch']

train_df2 = df2[~df2['Epoch'].str.contains('Validation')]
train_epochs2 = train_df2['Epoch']

train_df3 = df3[~df3['Epoch'].str.contains('Validation')]
train_epochs3 = train_df3['Epoch']

val_df1 = df1[df1['Epoch'].str.contains('Validation')]
val_epochs1 = val_df1['Epoch']

val_df2 = df2[df2['Epoch'].str.contains('Validation')]
val_epochs2 = val_df2['Epoch']

val_df3 = df3[df3['Epoch'].str.contains('Validation')]
val_epochs3 = val_df3['Epoch']

# Extract starting epoch
start_train1 = int(train_epochs1.iloc[0])
start_train2 = int(train_epochs2.iloc[0])
start_train3 = int(train_epochs3.iloc[0])

start_val1 = 50 #int(val_epochs1.iloc[0].split()[1])
start_val2 = 50 #int(val_epochs2.iloc[0].split()[1])
start_val3 = 50 #int(val_epochs3.iloc[0].split()[1])

# Calculate mean of metrics (Precision, Specificity, Sensitivity) from class 1 to 3
mean_precision_train1 = train_df1[['Precision (1)', 'Precision (2)', 'Precision (3)']].mean(axis=1)
mean_precision_train2 = train_df2[['Precision (1)', 'Precision (2)', 'Precision (3)']].mean(axis=1)
mean_precision_train3 = train_df3[['Precision (1)', 'Precision (2)', 'Precision (3)']].mean(axis=1)

mean_specificity_train1 = train_df1[['Specificity (1)', 'Specificity (2)', 'Specificity (3)']].mean(axis=1)
mean_specificity_train2 = train_df2[['Specificity (1)', 'Specificity (2)', 'Specificity (3)']].mean(axis=1)
mean_specificity_train3 = train_df3[['Specificity (1)', 'Specificity (2)', 'Specificity (3)']].mean(axis=1)

mean_sensitivity_train1 = train_df1[['Sensitivity (1)', 'Sensitivity (2)', 'Sensitivity (3)']].mean(axis=1)
mean_sensitivity_train2 = train_df2[['Sensitivity (1)', 'Sensitivity (2)', 'Sensitivity (3)']].mean(axis=1)
mean_sensitivity_train3 = train_df3[['Sensitivity (1)', 'Sensitivity (2)', 'Sensitivity (3)']].mean(axis=1)

mean_precision_val1 = val_df1[['Precision (1)', 'Precision (2)', 'Precision (3)']].mean(axis=1)
mean_precision_val2 = val_df2[['Precision (1)', 'Precision (2)', 'Precision (3)']].mean(axis=1)
mean_precision_val3 = val_df3[['Precision (1)', 'Precision (2)', 'Precision (3)']].mean(axis=1)

mean_specificity_val1 = val_df1[['Specificity (1)', 'Specificity (2)', 'Specificity (3)']].mean(axis=1)
mean_specificity_val2 = val_df2[['Specificity (1)', 'Specificity (2)', 'Specificity (3)']].mean(axis=1)
mean_specificity_val3 = val_df3[['Specificity (1)', 'Specificity (2)', 'Specificity (3)']].mean(axis=1)

mean_sensitivity_val1 = val_df1[['Sensitivity (1)', 'Sensitivity (2)', 'Sensitivity (3)']].mean(axis=1)
mean_sensitivity_val2 = val_df2[['Sensitivity (1)', 'Sensitivity (2)', 'Sensitivity (3)']].mean(axis=1)
mean_sensitivity_val3 = val_df3[['Sensitivity (1)', 'Sensitivity (2)', 'Sensitivity (3)']].mean(axis=1)

# Create subplots
fig, axes = plt.subplots(1, 3, figsize=(18, 6))

# Determine max epoch for x-axis limit
max_epoch = int(max(train_epochs3.iloc[-1], train_epochs2.iloc[-1], train_epochs1.iloc[-1]))

# Plot mean precision scores for training epochs
axes[0].plot(train_epochs1, mean_precision_train1, color='red', label='AR2B')
axes[0].plot(train_epochs2, mean_precision_train2, color='blue', label='AR2B_DeepSup')
axes[0].plot(train_epochs3, mean_precision_train3, color='green', label='Swin_AR2B_DeepSup')
axes[0].set_title('Mean Precision (Training Epochs)')
axes[0].set_ylabel('Mean Score')
axes[0].set_xlabel('Epoch')
axes[0].set_xticks(range(199, max_epoch + 1, 200))
axes[0].legend()

# Plot mean specificity scores for training epochs
axes[1].plot(train_epochs1, mean_specificity_train1, color='red', label='AR2B')
axes[1].plot(train_epochs2, mean_specificity_train2, color='blue', label='AR2B_DeepSup')
axes[1].plot(train_epochs3, mean_specificity_train3, color='green', label='Swin_AR2B_DeepSup')
axes[1].set_title('Mean Specificity (Training Epochs)')
axes[1].set_ylabel('Mean Score')
axes[1].set_xlabel('Epoch')
axes[1].set_xticks(range(199, max_epoch + 1, 200))
axes[1].legend()

# Plot mean sensitivity scores for training epochs
axes[2].plot(train_epochs1, mean_sensitivity_train1, color='red', label='AR2B')
axes[2].plot(train_epochs2, mean_sensitivity_train2, color='blue', label='AR2B_DeepSup')
axes[2].plot(train_epochs3, mean_sensitivity_train3, color='green', label='Swin_AR2B_DeepSup')
axes[2].set_title('Mean Sensitivity (Training Epochs)')
axes[2].set_ylabel('Mean Score')
axes[2].set_xlabel('Epoch')
axes[2].set_xticks(range(199, max_epoch + 1, 200))
axes[2].legend()

# Get maximum epochs
max_epoch = int(max(train_epochs3.iloc[-1], train_epochs2.iloc[-1], train_epochs1.iloc[-1]))

# Check for re-training condition
retrain_condition = any([int(train_epochs1.iloc[-1]) > 1000,
                         int(train_epochs2.iloc[-1]) > 1000,
                         int(train_epochs3.iloc[-1]) > 1000])

# Adjust for re-training
if retrain_condition:
    for ax in axes:
        ax.axvline(x=1000, linestyle='--', color='grey')
        ax.text(1010, ax.get_ylim()[0] + 0.05, 'Extended Training', fontsize=9, color='grey')

# Adjust layout
plt.tight_layout()

plt.show()