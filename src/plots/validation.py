import pandas as pd
import matplotlib.pyplot as plt

# Load CSV files
df1 = pd.read_csv('07_training_results.csv')
df2 = pd.read_csv('08_training_results.csv')
df3 = pd.read_csv('09_training_results.csv')

# Convert Epoch to strings
df1['Epoch'] = df1['Epoch'].astype(str)
df2['Epoch'] = df2['Epoch'].astype(str)
df3['Epoch'] = df3['Epoch'].astype(str)

# Filter dataframe and epochs
df1 = df1[df1['Epoch'].str.contains('Validation')]
epochs1 = df1['Epoch'][df1['Epoch'].str.contains('Validation')]

df2 = df2[df2['Epoch'].str.contains('Validation')]
epochs2 = df2['Epoch'][df2['Epoch'].str.contains('Validation')]

df3 = df3[df3['Epoch'].str.contains('Validation')]
epochs3 = df3['Epoch'][df3['Epoch'].str.contains('Validation')]

# Extract starting epoch
start1 = 50
start2 = 50
start3 = 50

# Extract data
acc1 = df1['Accuracy']
loss1 = df1['Loss']
acc2 = df2['Accuracy']
loss2 = df2['Loss']
acc3 = df3['Accuracy']
loss3 = df3['Loss']

# Create subplots
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))

# Plot accuracy
line_acc1, = ax1.plot(range(start1, len(epochs1) * 50 + start1, 50), acc1, color='red', label='AR2B')
line_acc2, = ax1.plot(range(start2, len(epochs2) * 50 + start2, 50), acc2, color='blue', label='AR2B_DeepSup')
line_acc3, = ax1.plot(range(start3, len(epochs3) * 50 + start3, 50), acc3, color='green', label='Swin_AR2B_DeepSup')
ax1.set_title('Validation Accuracy')
ax1.set_ylabel('Accuracy')
ax1.set_xlabel('Epoch')
ax1.set_ylim(bottom=0.5)
ax1.legend(loc='lower right')

# Find highest accuracy points
max_acc1 = acc1.max()
max_acc2 = acc2.max()
max_acc3 = acc3.max()

# Plot loss
line_loss1, = ax2.plot(range(start1, len(epochs1) * 50 + start1, 50), loss1, color='red', label='AR2B')
line_loss2, = ax2.plot(range(start2, len(epochs2) * 50 + start2, 50), loss2, color='blue', label='AR2B_DeepSup')
line_loss3, = ax2.plot(range(start3, len(epochs3) * 50 + start3, 50), loss3, color='green', label='Swin_AR2B_DeepSup')
ax2.set_title('Validation Loss')
ax2.set_ylabel('Loss')
ax2.set_xlabel('Epoch')
ax2.set_ylim(bottom=0)
ax2.legend(loc='lower right')

# Find lowest loss points
min_loss1 = loss1.min()
min_loss2 = loss2.min()
min_loss3 = loss3.min()

# Annotate highest accuracy points for both subplots
ax1.annotate(f'Max Acc: {max_acc1:.3f}', xy=(acc1.idxmax() - 10, max_acc1 - 0.002), xytext=(-40, -40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax1.annotate(f'Max Acc: {max_acc2:.3f}', xy=(acc2.idxmax() - 15, max_acc2), xytext=(-40, -100),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax1.annotate(f'Max Acc: {max_acc3:.3f}', xy=(acc3.idxmax() - 10, max_acc3), xytext=(-40, -80),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')

# Annotate lowest loss points for both subplots
ax2.annotate(f'CE Min Loss: {min_loss1:.3f}', xy=(loss1.idxmin(), min_loss1), xytext=(20, -20),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax2.annotate(f'CE+DL Min Loss: {min_loss2:.3f}', xy=(loss2.idxmin() - 10, min_loss2), xytext=(-90, -40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax2.annotate(f'CE+DL Min Loss: {min_loss3:.3f}', xy=(loss3.idxmin() - 10, min_loss3), xytext=(-90, -40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')


# Check for re-training condition
retrain_condition = any([int(len(epochs1) * 50)>1000,
                         int(len(epochs2) * 50)>1000,
                         int(len(epochs2) * 50)>1000])

# Show vertical lines if re-training
if retrain_condition:
  # Add vertical line and text for 1000 epochs on the accuracy plot
  ax1.axvline(x=1000, color='grey', linestyle='--', linewidth=0.7)
  y_pos_acc = 0.55 * acc1.max()  # You can adjust this based on your preference
  ax1.text(1010, y_pos_acc, 'Extended Training', fontsize=9, color='grey')

  # Add vertical line and text for 1000 epochs on the loss plot
  ax2.axvline(x=1000, color='grey', linestyle='--', linewidth=0.7)
  y_pos_loss = 0.85 * loss1.max()  # You can adjust this based on your preference
  ax2.text(1010, y_pos_loss, 'Extended Training', fontsize=9, color='grey')

# Adjust layout
plt.tight_layout()

plt.show()