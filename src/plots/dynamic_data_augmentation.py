import pandas as pd
import matplotlib.pyplot as plt

# Load CSV files
df1 = pd.read_csv('07_training_results.csv')
df2 = pd.read_csv('08_training_results.csv')
df3 = pd.read_csv('09_training_results.csv')
df4 = pd.read_csv('18_training_results.csv')  # Load the new CSV file

# Convert Epoch to strings
df1['Epoch'] = df1['Epoch'].astype(str)
df2['Epoch'] = df2['Epoch'].astype(str)
df3['Epoch'] = df3['Epoch'].astype(str)
df4['Epoch'] = df4['Epoch'].astype(str)  # Convert Epoch in the new dataframe

# Filter dataframe and epochs
df1 = df1[~df1['Epoch'].str.contains('Validation')]
epochs1 = df1['Epoch']

df2 = df2[~df2['Epoch'].str.contains('Validation')]
epochs2 = df2['Epoch']

df3 = df3[~df3['Epoch'].str.contains('Validation')]
epochs3 = df3['Epoch']

df4 = df4[~df4['Epoch'].str.contains('Validation')]  # Filter in the new dataframe
epochs4 = df4['Epoch']  # Extract epochs from the new dataframe

# Extract data
acc1 = df1['Accuracy']
loss1 = df1['Loss']
acc2 = df2['Accuracy']
loss2 = df2['Loss']
acc3 = df3['Accuracy']
loss3 = df3['Loss']
acc4 = df4['Accuracy']  # Extract accuracy from the new dataframe
loss4 = df4['Loss']  # Extract loss from the new dataframe

start1 = 0  # You can modify this if needed
start2 = 0  # You can modify this if needed
start3 = 0  # You can modify this if needed
start4 = 0  # You can modify this if needed

# Create subplots
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))

# Plot accuracy
line_acc1, = ax1.plot(range(start1, len(epochs1) + start1), acc1, color='red', label='AR2B')
line_acc2, = ax1.plot(range(start2, len(epochs2) + start2), acc2, color='blue', label='AR2B_DeepSup')
line_acc3, = ax1.plot(range(start3, len(epochs3) + start3), acc3, color='green', label='Swin_AR2B_DeepSup')
line_acc4, = ax1.plot(range(start4, len(epochs4) + start4), acc4, color='purple', label='AR2B_DeepSup w/ Augmentation')  # Plot the new model accuracy
ax1.set_title('Training Accuracy')
ax1.set_ylabel('Accuracy')
ax1.set_xlabel('Epoch')
ax1.legend()

# Find highest accuracy points
max_acc1 = acc1.max()
max_acc2 = acc2.max()
max_acc3 = acc3.max()
max_acc4 = acc4.max()  # Find highest accuracy for the new model

# Annotate highest points
ax1.annotate(f'Max Acc: {max_acc1:.2f}', xy=(acc1.idxmax(), max_acc1), xytext=(10, 20),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax1.annotate(f'Max Acc: {max_acc2:.2f}', xy=(acc2.idxmax(), max_acc2), xytext=(20, 0),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax1.annotate(f'Max Acc: {max_acc3:.2f}', xy=(acc3.idxmax(), max_acc3), xytext=(30, -20),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')
ax1.annotate(f'Max Acc: {max_acc4:.2f}', xy=(acc4.idxmax(), max_acc4), xytext=(40, 0),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='purple'),
             color='purple')

# Plot loss
line_loss1, = ax2.plot(range(start1, len(epochs1) + start1), loss1, color='red', label='AR2B')
line_loss2, = ax2.plot(range(start2, len(epochs2) + start2), loss2, color='blue', label='AR2B_DeepSup')
line_loss3, = ax2.plot(range(start3, len(epochs3) + start3), loss3, color='green', label='Swin_AR2B_DeepSup')
line_loss4, = ax2.plot(range(start4, len(epochs4) + start4), loss4, color='purple', label='AR2B_DeepSup w/ Augmentation')  # Plot the new model loss
ax2.set_title('Training Loss')
ax2.set_ylabel('Loss')
ax2.set_xlabel('Epoch')
ax2.legend()

# Find lowest loss points
min_loss1 = loss1.min()
min_loss2 = loss2.min()
min_loss3 = loss3.min()
min_loss4 = loss4.min()  # Find lowest loss for the new model

# Annotate lowest points
ax2.annotate(f'CE Min Loss: {min_loss1:.2f}', xy=(loss1.idxmin(), min_loss1), xytext=(10, 10),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax2.annotate(f'CE+DL Min Loss: {min_loss2:.2f}', xy=(loss2.idxmin(), min_loss2), xytext=(10, 20),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax2.annotate(f'CE+DL Min Loss: {min_loss3:.2f}', xy=(loss3.idxmin(), min_loss3), xytext=(10, 30),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')
ax2.annotate(f'CE+DL Min Loss: {min_loss4:.2f}', xy=(loss4.idxmin(), min_loss4), xytext=(10, 40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='purple'),
             color='purple')

# Adjust layout
plt.tight_layout()

plt.show()