import pandas as pd
import matplotlib.pyplot as plt

# Load CSV files
df1 = pd.read_csv('17_training_results.csv')
df2 = pd.read_csv('08_training_results.csv')
df3 = pd.read_csv('09_training_results.csv')

# Convert Epoch to strings
df1['Epoch'] = df1['Epoch'].astype(str)
df2['Epoch'] = df2['Epoch'].astype(str)
df3['Epoch'] = df3['Epoch'].astype(str)

# Filter dataframe and epochs
df1 = df1[df1['Epoch'].str.contains('Validation') == False]
epochs1 = df1['Epoch'][df1['Epoch'].str.contains('Validation') == False]

df2 = df2[df2['Epoch'].str.contains('Validation') == False]
epochs2 = df2['Epoch'][df2['Epoch'].str.contains('Validation') == False]

df3 = df3[df3['Epoch'].str.contains('Validation') == False]
epochs3 = df3['Epoch'][df3['Epoch'].str.contains('Validation') == False]

# Extract data
acc1 = df1['Accuracy']
loss1 = df1['Loss']
acc2 = df2['Accuracy']
loss2 = df2['Loss']
acc3 = df3['Accuracy']
loss3 = df3['Loss']

start1 = int(epochs1.iloc[0])
start2 = int(epochs2.iloc[0])
start3 = int(epochs3.iloc[0])

# Create subplots
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))

# Plot accuracy
line_acc1, = ax1.plot(range(start1, len(epochs1) + start1), acc1, color='red', label='AR2B')
line_acc2, = ax1.plot(range(start2, len(epochs2) + start2), acc2, color='blue', label='AR2B_DeepSup')
line_acc3, = ax1.plot(range(start3, len(epochs3) + start3), acc3, color='green', label='Swin_AR2B_DeepSup')
ax1.set_title('Training Accuracy')
ax1.set_ylabel('Accuracy')
ax1.set_xlabel('Epoch')
ax1.legend()

# Find highest accuracy points
max_acc1 = acc1.max()
max_acc2 = acc2.max()
max_acc3 = acc3.max()


# Plot loss
line_loss1, = ax2.plot(range(start1, len(epochs1) + start1), loss1, color='red', label='AR2B')
line_loss2, = ax2.plot(range(start2, len(epochs2) + start2), loss2, color='blue', label='AR2B_DeepSup')
line_loss3, = ax2.plot(range(start3, len(epochs3) + start3), loss3, color='green', label='Swin_AR2B_DeepSup')
ax2.set_title('Training Loss')
ax2.set_ylabel('Loss')
ax2.set_xlabel('Epoch')
#ax2.set_ylim(top=1.0)
ax2.legend()

# Find lowest loss points
min_loss1 = loss1.min()
min_loss2 = loss2.min()
min_loss3 = loss3.min()

# Annotate highest points on plot 1
ax1.annotate(f'Max Acc: {max_acc1:.3f}', xy=(acc1.idxmax(), max_acc1), xytext=(-100, -40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax1.annotate(f'Max Acc: {max_acc2:.3f}', xy=(acc2.idxmax(), max_acc2), xytext=(-80, -60),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax1.annotate(f'Max Acc: {max_acc3:.3f}', xy=(acc3.idxmax(), max_acc3), xytext=(-50, -80),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')

# Annotate lowest points on plot 2
ax2.annotate(f'Min Loss: {min_loss1:.3f}', xy=(loss1.idxmin(), min_loss1), xytext=(-87, 40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='red'),
             color='red')
ax2.annotate(f'Min Loss: {min_loss2:.3f}', xy=(loss2.idxmin(), min_loss2), xytext=(-108, 40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='blue'),
             color='blue')
ax2.annotate(f'Min Loss: {min_loss3:.3f}', xy=(loss3.idxmin(), min_loss3), xytext=(-105, 40),
             textcoords='offset points', arrowprops=dict(arrowstyle='->', color='green'),
             color='green')

# Check for re-training condition
retrain_condition = any([len(epochs1)>1000,
                         len(epochs2)>1000,
                         len(epochs2)>1000])

# Show vertical lines if re-training
if retrain_condition:
  # Add vertical line and text for 1000 epochs on the accuracy plot
  ax1.axvline(x=1000, color='grey', linestyle='--', linewidth=0.7)
  y_pos_acc = 0.25 * acc1.max()  # You can adjust this based on your preference
  ax1.text(1010, y_pos_acc, 'Extended Training', fontsize=9, color='grey')

  # Add vertical line and text for 1000 epochs on the loss plot
  ax2.axvline(x=1000, color='grey', linestyle='--', linewidth=0.7)
  y_pos_loss = 1.25 * loss1.max()  # You can adjust this based on your preference
  ax2.text(1010, y_pos_loss, 'Extended Training', fontsize=9, color='grey')




# Adjust layout
plt.tight_layout()

plt.show()
