# Create subplots
fig, axes = plt.subplots(1, 3, figsize=(18, 6))

# Plot mean precision scores for validation epochs
axes[0].plot(range(start_val1, len(val_epochs1) * 50 + start_val1, 50), mean_precision_val1, color='red', label='AR2B')
axes[0].plot(range(start_val2, len(val_epochs2) * 50 + start_val2, 50), mean_precision_val2, color='blue', label='AR2B_DeepSup')
axes[0].plot(range(start_val3, len(val_epochs3) * 50 + start_val3, 50), mean_precision_val3, color='green', label='Swin_AR2B_DeepSup')
axes[0].set_title('Mean Precision (Validation Epochs)')
axes[0].set_ylabel('Mean Score')
axes[0].set_xlabel('Epoch')
axes[0].set_ylim(0)
axes[0].legend(loc='lower right')

# Plot mean specificity scores for validation epochs
axes[1].plot(range(start_val1, len(val_epochs1) * 50 + start_val1, 50), mean_specificity_val1, color='red', label='AR2B')
axes[1].plot(range(start_val2, len(val_epochs2) * 50 + start_val2, 50), mean_specificity_val2, color='blue', label='AR2B_DeepSup')
axes[1].plot(range(start_val3, len(val_epochs3) * 50 + start_val3, 50), mean_specificity_val3, color='green', label='Swin_AR2B_DeepSup')
axes[1].set_title('Mean Specificity (Validation Epochs)')
axes[1].set_ylabel('Mean Score')
axes[1].set_xlabel('Epoch')
axes[1].set_ylim(0.9)
axes[1].legend(loc='lower right')

# Plot mean sensitivity scores for validation epochs
axes[2].plot(range(start_val1, len(val_epochs1) * 50 + start_val1, 50), mean_sensitivity_val1, color='red', label='AR2B')
axes[2].plot(range(start_val2, len(val_epochs2) * 50 + start_val2, 50), mean_sensitivity_val2, color='blue', label='AR2B_DeepSup')
axes[2].plot(range(start_val3, len(val_epochs3) * 50 + start_val3, 50), mean_sensitivity_val3, color='green', label='Swin_AR2B_DeepSup')
axes[2].set_title('Mean Sensitivity (Validation Epochs)')
axes[2].set_ylabel('Mean Score')
axes[2].set_xlabel('Epoch')
axes[2].set_ylim(0)
axes[2].legend(loc='lower right')

# Check for re-training condition
retrain_condition = any([len(val_epochs1) * 50 > 1000,
                         len(val_epochs2) * 50 > 1000,
                         len(val_epochs2) * 50 > 1000])

# Adjust for re-training
if retrain_condition:
    for ax in axes:
        ax.axvline(x=1000, linestyle='--', color='grey')
        ax.text(1010, ax.get_ylim()[0] + 0.05, 'Extended Training', fontsize=9, color='grey')


# Adjust layout
plt.tight_layout()

plt.show()